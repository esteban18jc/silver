#!/bin/bash

# Declare all file extensions
declare -a file_types=(
    "*.psd"
    "*.tif"
    "*.jpg"
    "*.png"
    "*.mp3"
    "*.wav"
    "*.fbx"
    "*.obj"
    "*.unity"
    "*.prefab"
    "*.asset"
)

git lfs install

for file_type in "${file_types[@]}"
do
    git lfs track "$file_type"
done

echo "Git LFS configuration completed."
